#!/usr/bin/perl
# filter of help2man output
use strict;
use warnings;

while ( <STDIN> ) {
    #multiline regex
    undef $/;
    # remove colon from the name
    s/(^\.SH\sDESCRIPTION\n.*\.IP\n)(cat)/$2/gmxs;
    s/(^\.PP\n)(Input\soptions.*)/.SH OPTIONS\n.SS $2/gmxs;
    s/(^Batch\smode\s\(default\s=\s2\)\n)(Mode.*)/$1\n         $2/gmxs;
    s/(^\.TP\n)(0)(\n)(see\snote.*)/           $2      $4/gmxs;
    s/(^\.TP\n)(1)(\n)(see\snote.*)/           $2      $4/gmxs;
    s/(^\.TP\n)(\(default\))(\n)(2\s{6}see\snote.*)/ $2 $4/gmxs;
    s/(^3\s{6}see\snote.*)/           $1/gmxs;
    s/(^4\s{6}see\snote.*)/           $1/gmxs;
    s/(^5\s{6}expand\s.*)/           $1/gmxs;
    # lineup "-f format" option of gmap
    s/(under\sOutput\stypes\)\:\n)(.*)\.PP\n/
                my $q = $1;
                my $p = $2;
                $p =~ s"(\,\n)"$1 "gmxs;
                $p = $q.' '.$p;
                /egmsx;
    #gsnap aligning
    s/(^\.TP\n)(\(default\))(\n\s+)(4\s{6}see\snote.*)/ $2 $4/gmxs;
    s/(^\.TP\n)(2)(\n)(see\snote.*)/           $2      $4/gmxs;
    s/(^\.TP\n)(3)(\n)(see\snote.*)/           $2      $4/gmxs;
    s/(^5\s{6}see\snote\s.*)/           $1/gmxs;
    s/(^Examples:\n\.TP)/\.TP\n$1/gmsx;
    s/(.*be\schanged)\n\.TP\n(by\sproviding.*)/$1 $2/gmsx;
    #gmap aligning
    #fix gmap_build header
    #s/(^Batch\smode\s\(default\s=\s2\)\n)(Mode.*)/$1\n         $2/gmxs;
    s/(^\.TP\n)(0)(\n)(mmap\s.*)/           $2      $4/gmxs;
    s/(^\.TP\n)(1)(\n)(mmap\s.*)/           $2      $4/gmxs;
    s/(^\.TP\n)(\(default\))(\n)(2\s{6}mmap.*)/ $2 $4/gmxs;
    s/(^3\s{6}allocate.*)/           $1/gmxs;
    s/(^4\s{6}allocate.*)/           $1/gmxs;
    s/(^5\s{6}allocate.*)/           $1/gmxs;
    s/(^\.SH\sDESCRIPTION\n)Unknown.*\.PP/$1/gmsx;
    print;
}
print "\n\.TP\nOther tools of GMAP suite are located in \/usr\/lib\/gmap";
