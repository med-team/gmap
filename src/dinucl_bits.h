/* $Id: eb04236e95cdc02fe7a65394255e1fe814df25d6 $ */
#ifndef DINUCL_BITS_INCLUDED
#define DINUCL_BITS_INCLUDED

#include "types.h"

extern const Genomecomp_T donor_gtgc_bits[];
extern const Genomecomp_T acceptor_ag_bits[];
extern const Genomecomp_T donor_at_bits[];
extern const Genomecomp_T acceptor_ac_bits[];

extern const Genomecomp_T antidonor_gtgc_bits[];
extern const Genomecomp_T antiacceptor_ag_bits[];
extern const Genomecomp_T antidonor_at_bits[];
extern const Genomecomp_T antiacceptor_ac_bits[];

#endif
