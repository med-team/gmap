/* $Id: c18a2070f56579bda279c2e2225d2a4fa1d8f7a8 $ */
#ifndef TRPATH_CONVERT_INCLUDED
#define TRPATH_CONVERT_INCLUDED

#include "bool.h"
#include "pass.h"
#include "support.h"

#include "transcriptome.h"
#include "ef64.h"

#include "shortread.h"
#include "stage1hr.h"
#include "knownsplicing.h"
#include "compress.h"

#include "list.h"
#include "intlistpool.h"
#include "uintlistpool.h"
#include "univcoord.h"
#include "listpool.h"
#include "pathpool.h"
#include "vectorpool.h"
#include "transcriptpool.h"
#include "hitlistpool.h"


extern void
Trpath_convert_sense (int *found_score,

		      List_T *incomplete_sense_paths_gplus, List_T *incomplete_sense_paths_gminus,
		      List_T *incomplete_antisense_paths_gplus, List_T *incomplete_antisense_paths_gminus,

		      List_T *sense_paths_gplus, List_T *sense_paths_gminus,
		      List_T *antisense_paths_gplus, List_T *antisense_paths_gminus,

		      List_T sense_trpaths, bool first_read_p,
		      Shortread_T queryseq, int querylength,
		      Stage1_T this, Knownsplicing_T knownsplicing,

		      Compress_T query_compress_fwd, Compress_T query_compress_rev,

		      Intlistpool_T intlistpool, Uintlistpool_T uintlistpool,
		      Univcoordlistpool_T univcoordlistpool, Listpool_T listpool,
		      Pathpool_T pathpool, Vectorpool_T vectorpool, Transcriptpool_T transcriptpool,
		      Hitlistpool_T hitlistpool, Pass_T pass, bool crossover_sense_p);

extern void
Trpath_convert_antisense (int *found_score,

			  List_T *incomplete_antisense_paths_gplus, List_T *incomplete_antisense_paths_gminus,
			  List_T *incomplete_sense_paths_gplus, List_T *incomplete_sense_paths_gminus,

			  List_T *antisense_paths_gplus, List_T *antisense_paths_gminus,
			  List_T *sense_paths_gplus, List_T *sense_paths_gminus,

			  List_T antisense_trpaths, bool first_read_p,
			  Shortread_T queryseq, int querylength,
			  Stage1_T this, Knownsplicing_T knownsplicing,

			  Compress_T query_compress_fwd, Compress_T query_compress_rev,
			  
			  Intlistpool_T intlistpool, Uintlistpool_T uintlistpool,
			  Univcoordlistpool_T univcoordlistpool, Listpool_T listpool,
			  Pathpool_T pathpool, Vectorpool_T vectorpool, Transcriptpool_T transcriptpool,
			  Hitlistpool_T hitlistpool, Pass_T pass, bool crossover_sense_p);

extern void
Trpath_convert_setup (Transcriptome_T transcriptome_in,
		      EF64_T chromosome_ef64_in, Genomebits_T genomebits_in);

extern void
Trpath_convert_pass2_setup (Support_T support_tables_in);

#endif
