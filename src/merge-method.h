/* $Id$ */
#ifndef MERGE_METHOD_INCLUDED
#define MERGE_METHOD_INCLUDED
#ifdef HAVE_CONFIG_H
#include "config.h"		/* For HAVE_64_BIT */
#endif


#if 1

/* Original algorithm */
#ifdef HAVE_AVX2
#define USE_SIMD_MERGE 1
#elif defined(HAVE_AVX512)
#define USE_SIMD_MERGE 1
#else
#define USE_HEAP_MERGE 1
#endif

#else

/* Might even be faster, since it is O(n log k) */
#define USE_HEAP_MERGE 1

#endif

#endif
