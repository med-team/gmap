static char rcsid[] = "$Id: 75d78df03b13850b85eb5337856e2a2139c9fe0c $";
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#ifndef HAVE_MEMCPY
#define memcpy(d,s,n) bcopy((s),(d),(n))
#endif

#include "pathpair-eval.h"
#include "path-eval.h"		/* For Path_mark_alignment */

#include "path-solve.h"
#include "path-fusion.h"
#include "path-trim.h"

#include <stdio.h>
#include <math.h>		/* For rint */
#include "fastlog.h"		/* For fasterexp */

#include "assert.h"
#include "list.h"
#include "genomebits_count.h"
#include "junction.h"
#include "mapq.h"
#include "outputtype.h"
#include "stage1hr.h"


static bool *circularp;
static bool *chrsubsetp;
static bool *altlocp;

static Outputtype_T output_type;
static bool splicingp;
static bool resolve_inner_p;
static bool want_random_p;
static bool allow_soft_clips_p;
static Chrpos_T max_insertlength;

static Transcriptome_T transcriptome;


/* Splice goodness now determined by Splice_accept_p */
/* #define OMIT_BAD_SPLICES 1 */

#define PROB_SLOP 0.10

#define NMATCHES_FACTOR 8
#define INSERTLENGTH_SLOP 20

#define INSERTLENGTH_FACTOR 1.5
#define OUTERLENGTH_FACTOR 1.5

#define INNER_NEEDED 15

#define MAX(a,b) (a > b) ? a : b
#define MIN(a,b) (a < b) ? a : b


/* Pathpair_uniq */
#ifdef DEBUG7
#define debug7(x) x
#else
#define debug7(x)
#endif

/* Pathpair_eval_and_sort */
#ifdef DEBUG8
#define debug8(x) x
#else
#define debug8(x)
#endif

/* Pathpair_local_cmp */
#ifdef DEBUG8A
#define debug8a(x) x
#else
#define debug8a(x)
#endif

#define T Path_T

#if 0
/* For DNA-seq reads, do not want to use insertlength or outerlength */
/* Duplicates with respect to method have already been taken care of */
/* Ignore sensedir, so we keep both sensedirs if they have equivalent matches */
static int
Pathpair_method_cmp (const void *x, const void *y) {
  Pathpair_T a = * (Pathpair_T *) x;
  Pathpair_T b = * (Pathpair_T *) y;

  int cmpL, cmpH;
  int nindels_a, nindels_b;
  int method_a, method_b;


  cmpL = Path_nmatches_cmp(a->pathL,b->pathL);
  cmpH = Path_nmatches_cmp(a->pathH,b->pathH);

  if (cmpL <= 0 && cmpH < 0) {
    return -1;
  } else if (cmpL < 0 && cmpH <= 0) {
    return -1;
  } else if (cmpL >= 0 && cmpH > 0) {
    return +1;
  } else if (cmpL > 0 && cmpH >= 0) {
    return +1;
  } else if (cmpL < 0 && cmpH > 0) {
    /* Conflicting information */
    return 0;
  } else if (cmpL > 0 && cmpH < 0) {
    /* Conflicting information */
    return 0;
  }

  nindels_a = Path_nindels(a->pathL) + Path_nindels(a->pathH);
  nindels_b = Path_nindels(b->pathL) + Path_nindels(b->pathH);

  if (nindels_a < nindels_b) {
    return -1;
  } else if (nindels_b < nindels_a) {
    return +1;

  } else {
    method_a = (int) a->pathL->method + (int) a->pathH->method;
    method_b = (int) b->pathL->method + (int) b->pathH->method;

    if (method_a > method_b) {
      return -1;
    } else if (method_b > method_a) {
      return +1;
    } else {
      return 0;
    }
  }
}
#endif


/* Used for re-sorting pathpairs in an overlapping group.  Uses full
   nmatches, not chopped */
static int
Pathpair_local_sort (const void *x, const void *y) {
  Pathpair_T a = * (Pathpair_T *) x;
  Pathpair_T b = * (Pathpair_T *) y;

  /* int cmpL, cmpH; */
  int nmatches_a, nmatches_b;

  /* int coverage_a, coverage_b; */
  int nsegments_a, nsegments_b;
  int nalts_a = 0, nalts_b = 0;
#ifdef OMIT_BAD_SPLICES
  int nbadsplices_a, nbadsplices_b;
#endif

  double junction_prob_a, junction_prob_b,
    end_prob_a, end_prob_b, end_prob;

  bool overlappingp_x, overlappingp_y;
  bool insertlength_knownp_x, insertlength_knownp_y,
    outerlength_knownp_x, outerlength_knownp_y;

  /* Fusions can eliminate good results.  Can judge all by their nmatches */
  bool fusionp_x = false, fusionp_y = false;

#if 0
  printf("Pathpair_local_sort comparing\n");
  Pathpair_print(a);
  Pathpair_print(b);
#endif

  if (a->pathL->fusion_querystart_junction != NULL ||
      a->pathL->fusion_queryend_junction != NULL ||
      a->pathH->fusion_querystart_junction != NULL ||
      a->pathH->fusion_queryend_junction != NULL) {
    fusionp_x = true;
  }
  if (b->pathL->fusion_querystart_junction != NULL ||
      b->pathL->fusion_queryend_junction != NULL ||
      b->pathH->fusion_querystart_junction != NULL ||
      b->pathH->fusion_queryend_junction != NULL) {
    fusionp_y = true;
  }

  if (fusionp_x == false && fusionp_y == true) {
    return -1;
  } else if (fusionp_y == false && fusionp_x == true) {
    return +1;
  }

  nmatches_a = a->pathL->nmatches + a->pathH->nmatches;
  nmatches_b = b->pathL->nmatches + b->pathH->nmatches;
  
  /* Cannot have slop in a sort */
  if (nmatches_a > nmatches_b) {
    return -1;
  } else if (nmatches_b > nmatches_a) {
    return +1;
  } else {
    /* Equal.  Resolve below */
  }

  /* For local sort (but not cmp), consider transcriptome guidance */
  if (a->transcript_concordant_p > b->transcript_concordant_p) {
    return -1;
  } else if (b->transcript_concordant_p > a->transcript_concordant_p) {
    return +1;
  }

#ifdef OMIT_BAD_SPLICES
  nbadsplices_a = Pathpair_nbadsplices(a);
  nbadsplices_b = Pathpair_nbadsplices(b);
#endif

#ifdef OMIT_BAD_SPLICES				/* Allow for local_cmp */
  if (nbadsplices_a < nbadsplices_b) {
    return -1;
  } else if (nbadsplices_b < nbadsplices_a) {
    return +1;
  }
#endif
    
#if 0
  /* For local comparison, use chopped_nmatches instead */
  cmpL = Path_nmatches_cmp(a->pathL,b->pathL);
  cmpH = Path_nmatches_cmp(a->pathH,b->pathH);
  debug8a(printf("cmpL %d, cmpH %d\n",cmpL,cmpH));
  if (cmpL <= 0 && cmpH < 0) {
    return -1;
  } else if (cmpL < 0 && cmpH <= 0) {
    return -1;
  } else if (cmpL >= 0 && cmpH > 0) {
    return +1;
  } else if (cmpL > 0 && cmpH >= 0) {
    return +1;
  }
#endif

#if 0
  /* Don't use chopped comparison for sort, since we need to compute it pairwise */
  if (nmatches_a > nmatches_b + NMATCHES_FACTOR) {
    /* Use nmatches instead of chopped */
  } else if (nmatches_b > nmatches_a + NMATCHES_FACTOR) {
    /* Use nmatches instead of chopped */
  } else {
    /* Resolve by adding the values */
    chopped_nmatches_a = Path_eval_nmatches_chopped(a->pathL) + Path_eval_nmatches_chopped(a->pathH);
    chopped_nmatches_b = Path_eval_nmatches_chopped(b->pathL) + Path_eval_nmatches_chopped(b->pathH);
    debug8a(printf("Chopped_nmatches: %d+%d vs %d+%d\n",
		  Path_eval_nmatches_chopped(a->pathL),Path_eval_nmatches_chopped(a->pathH),
		  Path_eval_nmatches_chopped(b->pathL),Path_eval_nmatches_chopped(b->pathH)));
    if (chopped_nmatches_a > chopped_nmatches_b) {
      return -1;
    } else if (chopped_nmatches_b > chopped_nmatches_a) {
      return +1;
    }
  }
#endif

#if 0
  overlappingp_x = Pathpair_overlappingp(a);
  overlappingp_y = Pathpair_overlappingp(b);
  if (overlappingp_x == false && overlappingp_y == true) {
    return -1;
  } else if (overlappingp_y == false && overlappingp_x == true) {
    return +1;
  }
#endif

  nsegments_a = Path_nsegments(a->pathL) + Path_nsegments(a->pathH);
  nsegments_b = Path_nsegments(b->pathL) + Path_nsegments(b->pathH);

  if (nsegments_a < nsegments_b) {
    return -1;
  } else if (nsegments_b < nsegments_a) {
    return +1;
  }

#if 0
  if (a->insertlength + INSERTLENGTH_SLOP < b->insertlength) {
    return -1;
  } else if (b->insertlength + INSERTLENGTH_SLOP < a->insertlength) {
    return +1;
  }
#endif

  if (a->pathL->qstart_alts != NULL) {
    nalts_a++;
  }
  if (a->pathL->qend_alts != NULL) {
    nalts_a++;
  }
  if (a->pathH->qstart_alts != NULL) {
    nalts_a++;
  }
  if (a->pathH->qend_alts != NULL) {
    nalts_a++;
  }

  if (b->pathL->qstart_alts != NULL) {
    nalts_b++;
  }
  if (b->pathL->qend_alts != NULL) {
    nalts_b++;
  }
  if (b->pathH->qstart_alts != NULL) {
    nalts_b++;
  }
  if (b->pathH->qend_alts != NULL) {
    nalts_b++;
  }

  if (nalts_a < nalts_b) {
    return -1;
  } else if (nalts_b < nalts_a) {
    return +1;
  }

#if 0
    /* Already compared sense/antisense */
    /* We want to keep all possible transcript results at a locus */
  if (junction_prob_a > junction_prob_b) {
    return -1;
  } else if (junction_prob_b > junction_prob_a) {
    return +1;
  }
#endif

  /* This allows splice ends win over nosplice ends */
  junction_prob_a = Path_avg_junction_prob(&end_prob_a,a->pathL);
  junction_prob_a += Path_avg_junction_prob(&end_prob,a->pathH);
  end_prob_a += end_prob;

  junction_prob_b = Path_avg_junction_prob(&end_prob_b,b->pathL);
  junction_prob_b += Path_avg_junction_prob(&end_prob,b->pathH);
  end_prob_b += end_prob;

  if (junction_prob_a > junction_prob_b + PROB_SLOP) {
    return -1;
  } else if (junction_prob_b > junction_prob_a + PROB_SLOP) {
    return +1;
#if 0
    /* End probs can be unreliable, depending on where the alignment ends, and whether splicep is called at the end of the read */
  } else if (end_prob_a > end_prob_b + PROB_SLOP) {
    return -1;
  } else if (end_prob_b > end_prob_a + PROB_SLOP) {
    return +1;
#endif
  }

#if 0
  /* Even with better control over long splices, we still want to
     favor shorter splices.  And longer splices can be found using
     two-pass mode */
  /* Prioritize outerlength before nmatches, which could vary by a single base at the end */
  outerlength_knownp_x = Pathpair_outerlength_knownp(a);
  outerlength_knownp_y = Pathpair_outerlength_knownp(b);
  if (outerlength_knownp_x == false) {
    /* Fall through */
  } else if (outerlength_knownp_y == false) {
    /* Fall through */
  } else if (a->outerlength * OUTERLENGTH_FACTOR < b->outerlength) {
    return -1;
  } else if (b->outerlength * OUTERLENGTH_FACTOR < a->outerlength) {
    return +1;
  }
#endif

  return 0;
}


/* Used for comparing two pathpairs.  Uses chopped nmatches (where
   chop boundaries are determined just for the given pair) and then
   full nmatches */
static int
Pathpair_local_cmp (Pathpair_T a, Pathpair_T b,
		    Compress_T query5_compress_fwd, Compress_T query5_compress_rev,
		    Compress_T query3_compress_fwd, Compress_T query3_compress_rev) {
  /* int cmpL, cmpH; */
  int chop_qstart_5, chop_qend_5, chop_qstart_3, chop_qend_3;
  int chopped_nmatches_a, chopped_nmatches_b;
  int nmatches_a, nmatches_b;

  int coverage_a, coverage_b;
  int nsegments_a, nsegments_b;
  int nalts_a = 0, nalts_b = 0;
#ifdef OMIT_BAD_SPLICES
  int nbadsplices_a, nbadsplices_b;
#endif

  double junction_prob_a, junction_prob_b,
    end_prob_a, end_prob_b, end_prob;

  bool overlappingp_x, overlappingp_y;
  bool insertlength_knownp_x, insertlength_knownp_y,
    outerlength_knownp_x, outerlength_knownp_y;

  /* Fusions can eliminate good results.  Can judge all by their nmatches */
  bool fusionp_x = false, fusionp_y = false;

#ifdef DEBUG8A
  printf("Pathpair_local_cmp comparing\n");
  Pathpair_print(a);
  Pathpair_print(b);
#endif

  if (a->pathL->fusion_querystart_junction != NULL ||
      a->pathL->fusion_queryend_junction != NULL ||
      a->pathH->fusion_querystart_junction != NULL ||
      a->pathH->fusion_queryend_junction != NULL) {
    fusionp_x = true;
  }
  if (b->pathL->fusion_querystart_junction != NULL ||
      b->pathL->fusion_queryend_junction != NULL ||
      b->pathH->fusion_querystart_junction != NULL ||
      b->pathH->fusion_queryend_junction != NULL) {
    fusionp_y = true;
  }

  if (fusionp_x == false && fusionp_y == true) {
    return -1;
  } else if (fusionp_y == false && fusionp_x == true) {
    return +1;
  }


  nmatches_a = a->pathL->nmatches + a->pathH->nmatches;
  nmatches_b = b->pathL->nmatches + b->pathH->nmatches;
  
  if (nmatches_a > nmatches_b) {
    return -1;
  } else if (nmatches_b > nmatches_a) {
    return +1;
  } else {
    /* Equal.  Resolve below */
  }

#if 0
  /* For cmp, report both transcript-guided and equivalent pseudogene */
  debug8a(printf("Comparing concordance %d vs %d\n",
		 a->transcript_concordant_p,b->transcript_concordant_p));
  if (a->transcript_concordant_p > b->transcript_concordant_p) {
    return -1;
  } else if (b->transcript_concordant_p > a->transcript_concordant_p) {
    return +1;
  }
#endif

#if 0
  /* For local comparison, use chopped_nmatches instead */
  cmpL = Path_nmatches_cmp(a->pathL,b->pathL);
  cmpH = Path_nmatches_cmp(a->pathH,b->pathH);
  debug8a(printf("cmpL %d, cmpH %d\n",cmpL,cmpH));
  if (cmpL <= 0 && cmpH < 0) {
    return -1;
  } else if (cmpL < 0 && cmpH <= 0) {
    return -1;
  } else if (cmpL >= 0 && cmpH > 0) {
    return +1;
  } else if (cmpL > 0 && cmpH >= 0) {
    return +1;
  }
#endif

#if 0
  /* Already considered nmatches above */
  if (nmatches_a > nmatches_b + NMATCHES_FACTOR) {
    /* Use nmatches instead of chopped */
  } else if (nmatches_b > nmatches_a + NMATCHES_FACTOR) {
    /* Use nmatches instead of chopped */
  } else {
    chop_qstart_5 = MAX(Intlist_head(a->path5->endpoints),Intlist_head(b->path5->endpoints));
    chop_qend_5 = MIN(Intlist_last_value(a->path5->endpoints),Intlist_last_value(b->path5->endpoints));
    chop_qstart_3 = MAX(Intlist_head(a->path3->endpoints),Intlist_head(b->path3->endpoints));
    chop_qend_3 = MIN(Intlist_last_value(a->path3->endpoints),Intlist_last_value(b->path3->endpoints));

    chopped_nmatches_a =
      Path_eval_nmatches_chopped(a->path5,chop_qstart_5,chop_qend_5,
				 query5_compress_fwd,query5_compress_rev) +
      Path_eval_nmatches_chopped(a->path3,chop_qstart_3,chop_qend_3,
				 query3_compress_fwd,query3_compress_rev);
    chopped_nmatches_b =
      Path_eval_nmatches_chopped(b->path5,chop_qstart_5,chop_qend_5,
				 query5_compress_fwd,query5_compress_rev) +
      Path_eval_nmatches_chopped(b->path3,chop_qstart_3,chop_qend_3,
				 query3_compress_fwd,query3_compress_rev);

    debug8a(printf("Chopped_nmatches: %d+%d vs %d+%d\n",
		   Path_eval_nmatches_chopped(a->path5,chop_qstart_5,chop_qend_5,
					      query5_compress_fwd,query5_compress_rev),
		   Path_eval_nmatches_chopped(a->path3,chop_qstart_3,chop_qend_3,
					      query3_compress_fwd,query3_compress_rev),
		   Path_eval_nmatches_chopped(b->path5,chop_qstart_5,chop_qend_5,
					      query5_compress_fwd,query5_compress_rev),
		   Path_eval_nmatches_chopped(b->path3,chop_qstart_3,chop_qend_3,
					      query3_compress_fwd,query3_compress_rev)));
    if (chopped_nmatches_a > chopped_nmatches_b) {
      debug8a(printf("Picking by chopped nmatches\n"));
      return -1;
    } else if (chopped_nmatches_b > chopped_nmatches_a) {
      debug8a(printf("Picking by chopped nmatches\n"));
      return +1;
    }
  }

  if (nmatches_a > nmatches_b) {
    debug8a(printf("Picking by nmatches\n"));
    return -1;
  } else if (nmatches_b > nmatches_a) {
    debug8a(printf("Picking by nmatches\n"));
    return +1;
  } else {
    /* Equal.  Resolve below */
  }
#endif


  /* Coverage with slop might be helpful after chopped nmatches */
  coverage_a = Path_coverage(a->pathL) + Path_coverage(a->pathH);
  coverage_b = Path_coverage(b->pathL) + Path_coverage(b->pathH);
  
  if (coverage_a > coverage_b + 20) {
    return -1;
  } else if (coverage_b > coverage_a + 20) {
    return +1;
  }

#if 0
  overlappingp_x = Pathpair_overlappingp(a);
  overlappingp_y = Pathpair_overlappingp(b);
  if (overlappingp_x == false && overlappingp_y == true) {
    debug8a(printf("Picking by overlapping\n"));
    return -1;
  } else if (overlappingp_y == false && overlappingp_x == true) {
    debug8a(printf("Picking by overlapping\n"));
    return +1;
  }
#endif

  nsegments_a = Path_nsegments(a->pathL) + Path_nsegments(a->pathH);
  nsegments_b = Path_nsegments(b->pathL) + Path_nsegments(b->pathH);

  if (nsegments_a < nsegments_b) {
    debug8a(printf("Picking by nsegments\n"));
    return -1;
  } else if (nsegments_b < nsegments_a) {
    debug8a(printf("Picking by nsegments\n"));
    return +1;
  }

#if 0
  if (a->insertlength + INSERTLENGTH_SLOP < b->insertlength) {
    debug8a(printf("Picking by insertlength\n"));
    return -1;
  } else if (b->insertlength + INSERTLENGTH_SLOP < a->insertlength) {
    debug8a(printf("Picking by insertlength\n"));
    return +1;
  }
#endif

  if (a->pathL->qstart_alts != NULL) {
    nalts_a++;
  }
  if (a->pathL->qend_alts != NULL) {
    nalts_a++;
  }
  if (a->pathH->qstart_alts != NULL) {
    nalts_a++;
  }
  if (a->pathH->qend_alts != NULL) {
    nalts_a++;
  }

  if (b->pathL->qstart_alts != NULL) {
    nalts_b++;
  }
  if (b->pathL->qend_alts != NULL) {
    nalts_b++;
  }
  if (b->pathH->qstart_alts != NULL) {
    nalts_b++;
  }
  if (b->pathH->qend_alts != NULL) {
    nalts_b++;
  }

  if (nalts_a < nalts_b) {
    debug8a(printf("Picking by nalts\n"));
    return -1;
  } else if (nalts_b < nalts_a) {
    debug8a(printf("Picking by nalts\n"));
    return +1;
  }

#if 0
    /* Already compared sense/antisense */
    /* We want to keep all possible transcript results at a locus */
  if (junction_prob_a > junction_prob_b) {
    return -1;
  } else if (junction_prob_b > junction_prob_a) {
    return +1;
  }
#endif

  /* This allows splice ends win over nosplice ends */
  junction_prob_a = Path_avg_junction_prob(&end_prob_a,a->pathL);
  junction_prob_a += Path_avg_junction_prob(&end_prob,a->pathH);
  end_prob_a += end_prob;

  junction_prob_b = Path_avg_junction_prob(&end_prob_b,b->pathL);
  junction_prob_b += Path_avg_junction_prob(&end_prob,b->pathH);
  end_prob_b += end_prob;

  if (junction_prob_a > junction_prob_b + PROB_SLOP) {
    debug8a(printf("Picking by junction prob\n"));
    return -1;
  } else if (junction_prob_b > junction_prob_a + PROB_SLOP) {
    debug8a(printf("Picking by junction prob\n"));
    return +1;
#if 0
    /* End probs can be unreliable, depending on where the alignment ends, and whether splicep is called at the end of the read */
  } else if (end_prob_a > end_prob_b + PROB_SLOP) {
    debug8a(printf("Picking by end prob %f vs %f\n",end_prob_a,end_prob_b));
    return -1;
  } else if (end_prob_b > end_prob_a + PROB_SLOP) {
    debug8a(printf("Picking by end prob %f vs %f\n",end_prob_a,end_prob_b));
    return +1;
#endif
  }

#if 0
  /* Even with better control over long splices, we still want to
     favor shorter splices.  And longer splices can be found using
     two-pass mode */
  /* Prioritize outerlength before nmatches, which could vary by a single base at the end */
  outerlength_knownp_x = Pathpair_outerlength_knownp(a);
  outerlength_knownp_y = Pathpair_outerlength_knownp(b);
  if (outerlength_knownp_x == false) {
    /* Fall through */
  } else if (outerlength_knownp_y == false) {
    /* Fall through */
  } else if (a->outerlength * OUTERLENGTH_FACTOR < b->outerlength) {
    debug8a(printf("Picking by outerlength\n"));
    return -1;
  } else if (b->outerlength * OUTERLENGTH_FACTOR < a->outerlength) {
    debug8a(printf("Picking by outerlength\n"));
    return +1;
  }
#endif

  debug8a(printf("Same\n"));
  return 0;
}



/* Need to compare using chopped regions, because of the issues in
   comparing altsplice against a single splice, and comparing
   different splice lengths */
static int
Pathpair_global_sort (const void *x, const void *y) {
  Pathpair_T a = * (Pathpair_T *) x;
  Pathpair_T b = * (Pathpair_T *) y;

  int cmpL, cmpH;
  int nmatches_a, nmatches_b;

  /* int coverage_a, coverage_b; */
  /* bool insertlength_knownp_x, insertlength_knownp_y; */
  Chrpos_T insertlength_x, insertlength_y;
  Chrpos_T outerlength_x, outerlength_y;

#ifdef OMIT_BAD_SPLICES
  int nbadsplices_a, nbadsplices_b;
#endif


#ifdef OMIT_BAD_SPLICES
  nbadsplices_a = Pathpair_nbadsplices(a);
  nbadsplices_b = Pathpair_nbadsplices(b);
#endif

  /* Fusions can eliminate good results.  Can judge all by their nmatches */
  bool fusionp_x = false, fusionp_y = false;
  if (a->pathL->fusion_querystart_junction != NULL ||
      a->pathL->fusion_queryend_junction != NULL ||
      a->pathH->fusion_querystart_junction != NULL ||
      a->pathH->fusion_queryend_junction != NULL) {
    fusionp_x = true;
  }
  if (b->pathL->fusion_querystart_junction != NULL ||
      b->pathL->fusion_queryend_junction != NULL ||
      b->pathH->fusion_querystart_junction != NULL ||
      b->pathH->fusion_queryend_junction != NULL) {
    fusionp_y = true;
  }

  if (fusionp_x == false && fusionp_y == true) {
    return -1;
  } else if (fusionp_y == false && fusionp_x == true) {
    return +1;
  }

  cmpL = Path_nmatches_cmp(a->pathL,b->pathL);
  cmpH = Path_nmatches_cmp(a->pathH,b->pathH);

  if (cmpL <= 0 && cmpH < 0) {
    return -1;
  } else if (cmpL < 0 && cmpH <= 0) {
    return -1;
  } else if (cmpL >= 0 && cmpH > 0) {
    return +1;
  } else if (cmpL > 0 && cmpH >= 0) {
    return +1;
  }

  nmatches_a = a->pathL->nmatches + a->pathH->nmatches;
  nmatches_b = b->pathL->nmatches + b->pathH->nmatches;

  if (nmatches_a > nmatches_b) {
    return -1;
  } else if (nmatches_b > nmatches_a) {
    return +1;
  } else {
    /* Equal.  Resolve below */
  }

#if 0
  /* Cannot have slop in a sort */
  coverage_a = Path_coverage(a->pathL) + Path_coverage(a->pathH);
  coverage_b = Path_coverage(b->pathL) + Path_coverage(b->pathH);

  if (coverage_a > coverage_b + 20) {
    return -1;
  } else if (coverage_b > coverage_a + 20) {
    return +1;
  }
#endif

#ifdef OMIT_BAD_SPLICES
    /* Do not consider bad splices for global_cmp */
  if (nbadsplices_a < nbadsplices_b) {
    return -1;
  } else if (nbadsplices_b < nbadsplices_a) {
    return +1;
  }
#endif
    
#if 0
  /* Don't use chopped comparison for sort, since we need to compute it pairwise */
  /* Resolve by adding the values */
  if (nmatches_a > nmatches_b + NMATCHES_FACTOR) {
    /* Use nmatches instead of chopped */
  } else if (nmatches_b > nmatches_a + NMATCHES_FACTOR) {
    /* Use nmatches instead of chopped */
  } else {
    chopped_nmatches_a = Path_eval_nmatches_chopped(a->pathL) + Path_eval_nmatches_chopped(a->pathH);
    chopped_nmatches_b = Path_eval_nmatches_chopped(b->pathL) + Path_eval_nmatches_chopped(b->pathH);
    if (chopped_nmatches_a > chopped_nmatches_b) {
      return -1;
    } else if (chopped_nmatches_b > chopped_nmatches_a) {
      return +1;
    }
  }
#endif

  /* For sort (but not cmp), consider transcript guidance */
  if (a->transcript_concordant_p > b->transcript_concordant_p) {
    return -1;
  } else if (b->transcript_concordant_p > a->transcript_concordant_p) {
    return +1;
  }

  /* For sort (but not cmp), consider insertlength */
  if (splicingp == false) {
    return 0;

  } else {
    /* We want to keep all possible transcript results across loci */
    /* insertlength_knownp_x = Pathpair_insertlength_knownp(a); */
    /* insertlength_knownp_y = Pathpair_insertlength_knownp(b); */
    insertlength_x = Pathpair_insertlength(a);
    insertlength_y = Pathpair_insertlength(b);

    if (insertlength_x > max_insertlength && insertlength_y > max_insertlength) {
      /* Don't use insertlength */
    } else if (insertlength_x * INSERTLENGTH_FACTOR < insertlength_y) {
      return -1;
    } else if (insertlength_y * INSERTLENGTH_FACTOR < insertlength_x) {
      return +1;
    } else {
      return 0;
    }

    outerlength_x = Path_genomichigh(a->pathH) - Path_genomiclow(a->pathL);
    outerlength_y = Path_genomichigh(b->pathH) - Path_genomiclow(b->pathL);

    if (outerlength_x * OUTERLENGTH_FACTOR < outerlength_y) {
      return -1;
    } else if (outerlength_y * OUTERLENGTH_FACTOR < outerlength_x) {
      return +1;
    } else {
      return 0;
    }
  }
}


static int
Pathpair_global_cmp (Pathpair_T a, Pathpair_T b,
		    Compress_T query5_compress_fwd, Compress_T query5_compress_rev,
		    Compress_T query3_compress_fwd, Compress_T query3_compress_rev) {
  int cmpL, cmpH;
  int chop_qstart_5, chop_qend_5, chop_qstart_3, chop_qend_3;
  int chopped_nmatches_a, chopped_nmatches_b;
  int nmatches_a, nmatches_b;

  int coverage_a, coverage_b;
  /* bool insertlength_knownp_x, insertlength_knownp_y; */
  Chrpos_T insertlength_x, insertlength_y;
  Chrpos_T outerlength_x, outerlength_y;

#ifdef OMIT_BAD_SPLICES
  int nbadsplices_a, nbadsplices_b;
#endif


#ifdef OMIT_BAD_SPLICES
  nbadsplices_a = Pathpair_nbadsplices(a);
  nbadsplices_b = Pathpair_nbadsplices(b);
#endif

  /* Fusions can eliminate good results.  Can judge all by their nmatches */
  bool fusionp_x = false, fusionp_y = false;
  if (a->pathL->fusion_querystart_junction != NULL ||
      a->pathL->fusion_queryend_junction != NULL ||
      a->pathH->fusion_querystart_junction != NULL ||
      a->pathH->fusion_queryend_junction != NULL) {
    fusionp_x = true;
  }
  if (b->pathL->fusion_querystart_junction != NULL ||
      b->pathL->fusion_queryend_junction != NULL ||
      b->pathH->fusion_querystart_junction != NULL ||
      b->pathH->fusion_queryend_junction != NULL) {
    fusionp_y = true;
  }

  if (fusionp_x == false && fusionp_y == true) {
    return -1;
  } else if (fusionp_y == false && fusionp_x == true) {
    return +1;
  }

#ifdef OMIT_BAD_SPLICES
    /* Do not consider bad splices for global_cmp */
  if (nbadsplices_a < nbadsplices_b) {
    return -1;
  } else if (nbadsplices_b < nbadsplices_a) {
    return +1;
  }
#endif
    
  cmpL = Path_nmatches_cmp(a->pathL,b->pathL);
  cmpH = Path_nmatches_cmp(a->pathH,b->pathH);

  if (cmpL <= 0 && cmpH < 0) {
    debug8a(printf("Picking by cmp\n"));
    return -1;
  } else if (cmpL < 0 && cmpH <= 0) {
    debug8a(printf("Picking by cmp\n"));
    return -1;
  } else if (cmpL >= 0 && cmpH > 0) {
    debug8a(printf("Picking by cmp\n"));
    return +1;
  } else if (cmpL > 0 && cmpH >= 0) {
    debug8a(printf("Picking by cmp\n"));
    return +1;
  }

  nmatches_a = a->pathL->nmatches + a->pathH->nmatches;
  nmatches_b = b->pathL->nmatches + b->pathH->nmatches;

  /* Previously relied on NMATCHES_FACTOR */
  if (nmatches_a > nmatches_b) {
    /* Use nmatches instead of chopped */
    debug8a(printf("Picking by nmatches and factor\n"));
    return -1;
  } else if (nmatches_b > nmatches_a) {
    /* Use nmatches instead of chopped */
    debug8a(printf("Picking by nmatches and factor\n"));
    return +1;
  }

#if 0
  /* Previously used chopped nmatches for global cmp */
  chop_qstart_5 = MAX(Intlist_head(a->path5->endpoints),Intlist_head(b->path5->endpoints));
  chop_qend_5 = MIN(Intlist_last_value(a->path5->endpoints),Intlist_last_value(b->path5->endpoints));
  chop_qstart_3 = MAX(Intlist_head(a->path3->endpoints),Intlist_head(b->path3->endpoints));
  chop_qend_3 = MIN(Intlist_last_value(a->path3->endpoints),Intlist_last_value(b->path3->endpoints));
  
  chopped_nmatches_a =
    Path_eval_nmatches_chopped(a->path5,chop_qstart_5,chop_qend_5,
			       query5_compress_fwd,query5_compress_rev) +
    Path_eval_nmatches_chopped(a->path3,chop_qstart_3,chop_qend_3,
			       query3_compress_fwd,query3_compress_rev);
  chopped_nmatches_b =
    Path_eval_nmatches_chopped(b->path5,chop_qstart_5,chop_qend_5,
			       query5_compress_fwd,query5_compress_rev) +
    Path_eval_nmatches_chopped(b->path3,chop_qstart_3,chop_qend_3,
			       query3_compress_fwd,query3_compress_rev);
  
  debug8a(printf("Chopped_nmatches: %d+%d vs %d+%d\n",
		 Path_eval_nmatches_chopped(a->path5,chop_qstart_5,chop_qend_5,
					    query5_compress_fwd,query5_compress_rev),
		 Path_eval_nmatches_chopped(a->path3,chop_qstart_3,chop_qend_3,
					    query3_compress_fwd,query3_compress_rev),
		 Path_eval_nmatches_chopped(b->path5,chop_qstart_5,chop_qend_5,
					    query5_compress_fwd,query5_compress_rev),
		 Path_eval_nmatches_chopped(b->path3,chop_qstart_3,chop_qend_3,
					    query3_compress_fwd,query3_compress_rev)));
  if (chopped_nmatches_a > chopped_nmatches_b) {
    debug8a(printf("Picking by chopped nmatches\n"));
    return -1;
  } else if (chopped_nmatches_b > chopped_nmatches_a) {
    debug8a(printf("Picking by chopped nmatches\n"));
    return +1;
  }


  /* Coverage with slop might be helpful after chopped nmatches */
  coverage_a = Path_coverage(a->pathL) + Path_coverage(a->pathH);
  coverage_b = Path_coverage(b->pathL) + Path_coverage(b->pathH);

  if (coverage_a > coverage_b + 20) {
    return -1;
  } else if (coverage_b > coverage_a + 20) {
    return +1;
  }
#endif


#if 0
  /* For cmp, consider transcript guided and equivalent pseudogene equally */
  if (a->transcript_concordant_p > b->transcript_concordant_p) {
    return -1;
  } else if (b->transcript_concordant_p > a->transcript_concordant_p) {
    return +1;
  }
#endif

#if 0
  if (splicingp == false) {
    return 0;

  } else {
    /* We want to keep all possible transcript results across loci */
    /* insertlength_knownp_x = Pathpair_insertlength_knownp(a); */
    /* insertlength_knownp_y = Pathpair_insertlength_knownp(b); */
    insertlength_x = Pathpair_insertlength(a);
    insertlength_y = Pathpair_insertlength(b);

    if (insertlength_x > max_insertlength && insertlength_y > max_insertlength) {
      /* Don't use insertlength */
    } else if (insertlength_x * INSERTLENGTH_FACTOR < insertlength_y) {
      return -1;
    } else if (insertlength_y * INSERTLENGTH_FACTOR < insertlength_x) {
      return +1;
    } else {
      return 0;
    }

    outerlength_x = Path_genomichigh(a->pathH) - Path_genomiclow(a->pathL);
    outerlength_y = Path_genomichigh(b->pathH) - Path_genomiclow(b->pathL);

    if (outerlength_x * OUTERLENGTH_FACTOR < outerlength_y) {
      return -1;
    } else if (outerlength_y * OUTERLENGTH_FACTOR < outerlength_x) {
      return +1;
    } else {
      return 0;
    }
  }
#endif

  return 0;
}



/* Accumulates pathpairs into the given list called pathpairs */
List_T
Pathpair_uniq (List_T pathpairs, Pathpair_T *pathpairarray, int npaths,
	       Intlistpool_T intlistpool, Univcoordlistpool_T univcoordlistpool,
	       Listpool_T listpool, Pathpool_T pathpool, Transcriptpool_T transcriptpool,
	       Hitlistpool_T hitlistpool) {

  int i, j, k, l, ll;
  Pathpair_T pathpair;

  bool *duplicatep;

  
#ifdef DEBUG7
  printf("Entered Pathpair_uniq with %d pathpairs\n",npaths);
  for (int pathi = 0; pathi < npaths; pathi++) {
    printf("Pathpair (sense %d and %d): %p and %p\n",
	   pathpairarray[pathi]->pathL->sensedir,pathpairarray[pathi]->pathH->sensedir,
	   pathpairarray[pathi]->pathL,pathpairarray[pathi]->pathH);
    Pathpair_print(pathpairarray[pathi]);
  }
  printf("\n");
#endif

  /* 0.  Sort by structure to remove duplicates */
  if (npaths == 0) {
    return pathpairs;

  } else {
    duplicatep = (bool *) CALLOC(npaths,sizeof(bool));
    qsort(pathpairarray,npaths,sizeof(Pathpair_T),Pathpair_interval_cmp);

    k = 0;
    i = 0;
    while (i < npaths) {
      j = i + 1;
      while (j < npaths && Pathpair_overlap_p(pathpairarray[j],pathpairarray[i]) == true) {
	j++;
      }
      
      if (j - i == 1) {
	pathpairarray[k++] = pathpairarray[i];

      } else {
	debug7(printf("Found an overlapping group of %d => eliminating duplicates\n",j - i));
	for (l = i; l < j; l++) {
	  if (duplicatep[l] == true) {
	    /* Already found to be a duplicate */
	  } else {
	    for (ll = l + 1; ll < j; ll++) {
	      /* Need to keep sensedirs separate so we can extend them separately */
	      if (Pathpair_structure_cmp(&(pathpairarray[l]),&(pathpairarray[ll])) == 0) {
		debug7(printf("Path %d is a duplicate of %d\n",ll,l));
		duplicatep[ll] = true;
	      }
	    }
	  }
	}

	for (l = i; l < j; l++) {
	  pathpair = pathpairarray[l];
	  if (duplicatep[l] == false) {
	    pathpairarray[k++] = pathpair;
	  } else {
	    Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			  listpool,pathpool,transcriptpool,hitlistpool);
	  }
	}
      }
      
      i = j;
    }

    FREE(duplicatep);
    npaths = k;
  }

  debug7(printf("(1) After removing duplicates (except for sensedir), have %d paths\n",npaths));


  for (i = 0; i < npaths; i++) {
    /* Set chopped regions and evaluate for nmatches */
    pathpairs = Hitlist_push(pathpairs,hitlistpool,(void *) pathpairarray[i]
			     hitlistpool_trace(__FILE__,__LINE__));
  }

  return pathpairs;
}


/* filtering is creating an issue between pathpairs != NULL, stopping
   path creation, and then an empty pathpairarray */

Pathpair_T *
Pathpair_eval_and_sort (int *found_score_5, int *found_score_3,
			int *npaths_primary, int *npaths_altloc, int *first_absmq, int *second_absmq,
			Pathpair_T *pathpairarray, int npaths, Stage1_T stage1_5, Stage1_T stage1_3,
			int streamspace_max_alloc, Univcoord_T *streamspace_alloc,
			Compress_T query5_compress_fwd, Compress_T query5_compress_rev,
			Compress_T query3_compress_fwd, Compress_T query3_compress_rev,
			Shortread_T queryseq5, Shortread_T queryseq3,
			char *queryuc_ptr_5, char *queryrc5, char *queryuc_ptr_3, char *queryrc3, 
			char *quality_string_5, char *quality_string_3,
			int *mismatch_positions_alloc_5, int *mismatch_positions_alloc_3,
			Univdiag_T *novel_univdiags_alloc, Univcoord_T *novel_diagonals_alloc,
			unsigned short *localdb_alloc, Knownsplicing_T knownsplicing,
			Knownindels_T knownindels,

			int nmismatches_allowed_5, int nmismatches_allowed_3,
			int nmismatches_filter_5, int nmismatches_filter_3,
			int mincoverage_filter_5, int mincoverage_filter_3,
			int querylength5, int querylength3,
			Univdiagpool_T univdiagpool, Intlistpool_T intlistpool,
			Uintlistpool_T uintlistpool, Univcoordlistpool_T univcoordlistpool,
			Listpool_T listpool, Pathpool_T pathpool, Transcriptpool_T transcriptpool,
			Vectorpool_T vectorpool, Hitlistpool_T hitlistpool,
			Pass_T pass, bool filterp) {
  int nanti, nsense, pathi;
  float maxlik, loglik;

  float total, qual;
  int mapq_score;

  int randomi, i, j, k, l, ll;
  Pathpair_T temp, pathpair, new_pathpair;
  Path_T path5, path3;
  Univcoord_T high, low, distal5, distal3;
  int tr_insertlength;

  bool *duplicatep;
  bool valid5p, valid3p;
  bool possible_fusion5_p = false, possible_fusion3_p = false;

  List_T paths, fusion_paths_5, fusion_paths_3, p, q;
  List_T unextended_pathpairs;
  double best_junction_prob_sense, best_junction_prob_antisense,
    best_end_prob_sense, best_end_prob_antisense,
    junction_prob, end_prob, prob;

  
  debug8(printf("Entered Pathpair_eval_and_sort with %d pathpairs\n",npaths));

  if (npaths == 0) {
    /* Skip */
    *npaths_primary = *npaths_altloc = 0;
    *first_absmq = 0;
    *second_absmq = 0;
    return (Pathpair_T *) NULL;
  }

  /* Have already called Path_extend */
#ifdef DEBUG8
  printf("In Pathpair_eval_and_sort, have %d pathpairs\n",npaths);
  for (pathi = 0; pathi < npaths; pathi++) {
    printf("Pathpair (sense %d and %d): %p and %p\n",
	   pathpairarray[pathi]->pathL->sensedir,pathpairarray[pathi]->pathH->sensedir,
	   pathpairarray[pathi]->pathL,pathpairarray[pathi]->pathH);
    Pathpair_print(pathpairarray[pathi]);
  }
  printf("\n");
#endif


  /* 0.  Sort by structure to remove duplicates.  Keep sensedirs separate */
  if (npaths > 1) {
    /* Keep sensedirs separate */
    qsort(pathpairarray,npaths,sizeof(Pathpair_T),Pathpair_sensedir_cmp);

    nanti = nsense = 0;
    for (i = 0; i < npaths; i++) {
      pathpair = pathpairarray[i];
      if (Pathpair_sensedir(pathpair) == SENSE_ANTI) {
	nanti++;
      } else if (Pathpair_sensedir(pathpair) == SENSE_FORWARD) {
	nsense++;
      } else {
	abort();
      }
    }
      
    duplicatep = (bool *) CALLOC(npaths,sizeof(bool));
    k = 0;

    /* 0a. Handle SENSE_ANTI (< SENSE_FORWARD) */
    qsort(pathpairarray,nanti,sizeof(Pathpair_T),Pathpair_interval_cmp);
    i = 0;

    while (i < nanti) {
      j = i + 1;
      while (j < nanti && Pathpair_overlap_p(pathpairarray[j],pathpairarray[i]) == true) {
	j++;
      }
      
      if (j - i == 1) {
	pathpairarray[k++] = pathpairarray[i];

      } else {
	debug8(printf(">Found an overlapping antisense group of %d => eliminating duplicates.  Re-sorting by goodness\n",j - i));
	qsort(&(pathpairarray[i]),j - i,sizeof(Pathpair_T),Pathpair_local_sort);

#ifdef DEBUG8
	printf("After sorting locally, have %d pathpairs\n",j - i);
	for (l = i; l < j; l++) {
	  printf("Path %d: ",l); Pathpair_print(pathpairarray[l]);
	}
	printf("\n");
#endif

	for (l = i; l < j; l++) {
	  if (duplicatep[l] == true) {
	    /* Already found to be a duplicate */
	  } else {
	    for (ll = l + 1; ll < j; ll++) {
	      if (Pathpair_structure_cmp(&(pathpairarray[l]),&(pathpairarray[ll])) == 0) {
		debug8(printf("Path %d is a duplicate of %d\n",ll,l));
		duplicatep[ll] = true;
	      }
	    }
	  }
	}

	for (l = i; l < j; l++) {
	  pathpair = pathpairarray[l];
	  if (duplicatep[l] == false) {
	    pathpairarray[k++] = pathpair;
	  } else {
	    Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			  listpool,pathpool,transcriptpool,hitlistpool);
	  }
	}
      }
      
      i = j;
    }


    /* 0b. Handle SENSE_FORWARD (> SENSE_ANTI) */
    qsort(&(pathpairarray[nanti]),nsense,sizeof(Pathpair_T),Pathpair_interval_cmp);
    i = nanti;

    while (i < npaths) {
      j = i + 1;
      while (j < npaths && Pathpair_overlap_p(pathpairarray[j],pathpairarray[i]) == true) {
	j++;
      }
      
      if (j - i == 1) {
	pathpairarray[k++] = pathpairarray[i];

      } else {
	debug8(printf(">Found an overlapping sense group of %d => eliminating duplicates.  Re-sorting by goodness\n",j - i));
	qsort(&(pathpairarray[i]),j - i,sizeof(Pathpair_T),Pathpair_local_sort);

#ifdef DEBUG8
	printf("After sorting locally, have %d pathpairs\n",j - i);
	for (l = i; l < j; l++) {
	  printf("Path %d: ",l); Pathpair_print(pathpairarray[l]);
	}
	printf("\n");
#endif

	for (l = i; l < j; l++) {
	  if (duplicatep[l] == true) {
	    /* Already found to be a duplicate */
	  } else {
	    for (ll = l + 1; ll < j; ll++) {
	      if (Pathpair_structure_cmp(&(pathpairarray[l]),&(pathpairarray[ll])) == 0) {
		debug8(printf("Path %d is a duplicate of %d\n",ll,l));
		duplicatep[ll] = true;
	      }
	    }
	  }
	}

	for (l = i; l < j; l++) {
	  pathpair = pathpairarray[l];
	  if (duplicatep[l] == false) {
	    pathpairarray[k++] = pathpair;
	  } else {
	    Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			  listpool,pathpool,transcriptpool,hitlistpool);
	  }
	}
      }
      
      i = j;
    }

    FREE(duplicatep);
    npaths = k;
  }

  debug8(printf("(0) After removing antisense and sense duplicates separately, have %d paths\n",npaths));
#ifdef DEBUG8
  for (pathi = 0; pathi < npaths; pathi++) {
    printf("Pathpair (sense %d and %d): %p and %p\n",
	   pathpairarray[pathi]->pathL->sensedir,pathpairarray[pathi]->pathH->sensedir,
	   pathpairarray[pathi]->pathL,pathpairarray[pathi]->pathH);
    Pathpair_print(pathpairarray[pathi]);
  }
  printf("\n");
#endif


  /* 1.  Unalias circular alignments and trim chrbounds */
  k = 0;
  for (i = 0; i < npaths; i++) {
    pathpair = pathpairarray[i];
    path5 = pathpair->path5;
    path3 = pathpair->path3;
    if (circularp[pathpair->pathL->chrnum] == true) {
      Path_trim_circular_unalias_pair(pathpair->path5,pathpair->path3,
				      query5_compress_fwd,query5_compress_rev,
				      query3_compress_fwd,query3_compress_rev);
    }
    /* true means that endpoints were acceptable */
    if (Path_trim_chrbounds(path5,query5_compress_fwd,query5_compress_rev,
			    intlistpool,univcoordlistpool,listpool,pathpool,
			    transcriptpool) == true &&
	Path_trim_chrbounds(path3,query3_compress_fwd,query3_compress_rev,
			    intlistpool,univcoordlistpool,listpool,pathpool,
			    transcriptpool) == true) {
      pathpairarray[k++] = pathpair;
    } else {
      debug8(printf("Eliminating due to chrbounds: ")); debug8(Pathpair_print(pathpair));
      Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
		    listpool,pathpool,transcriptpool,hitlistpool);
    }
  }

  npaths = k;
  debug8(printf("(1) After trimming chrbounds, have %d paths\n",npaths));
  if (npaths == 0) {
    FREE_OUT(pathpairarray);
    *npaths_primary = *npaths_altloc = 0;
    *first_absmq = 0;
    *second_absmq = 0;
    return (Pathpair_T *) NULL;
  }


  /* 2.  Trim and resolve inner splices */
  /* Previously sorted, but not necessary to group the pathpairs, and we sort at step 4 anyway */
  /* qsort(pathpairarray,npaths,sizeof(Pathpair_T),Pathpair_interval_cmp); */
  
  if (resolve_inner_p == true && npaths < 100) {
    /* Do not try to resolve, which is time-comsuming, if there are many pathpairs */
    /* Note: Need to handle cases where the reads overlap, so trimming needs to be determined by the distal univdiagonals */
    k = 0;
    for (i = 0; i < npaths; i++) {
      pathpair = pathpairarray[i];
      path5 = pathpair->path5;
      path3 = pathpair->path3;
      valid5p = valid3p = true;
      
      debug8(printf("Resolving paths %p and %p\n",path5,path3));
      if (pathpair->plusp == true) {
	distal5 = Path_genomiclow(path5);
	distal3 = Path_genomichigh(path3);
	
	/* Need to shift 5' univdiagonals down by querylength5 */
	if (Univcoordlist_last_value(path5->univdiagonals) < (Univcoord_T) querylength5) {
	  high = 0;
	} else {
	  high = Univcoordlist_last_value(path5->univdiagonals) - querylength5;
	}
	debug8(printf("high5 %u vs distal3 %u\n",high,distal3));

	if (0 && path5->transcriptome_method_p == true) {
	  /* Previously did not trim, but we could have added a splice to the end */
	} else if (high > distal3) { /* was path3->main_univdiagonal */
	  debug8(printf("Need to trim high part of 5' read, since %u > %u\n",high,distal3));
	  debug8(Path_print(path5));
	  debug8(Path_print(path3));
	  valid5p = Path_trim_qend_trimdiag(path5,query5_compress_fwd,query5_compress_rev,
					    intlistpool,univcoordlistpool,
					    listpool,pathpool,transcriptpool,
					    querylength5,/*trimdiag*/distal3);
	  debug8(printf("Result of trimming\n"));
	  debug8(Path_print(path5));
	}
	
	low = Univcoordlist_head(path3->univdiagonals);
	debug8(printf("low3 %u vs distal5 %u\n",low,distal5));

	if (0 && path3->transcriptome_method_p == true) {
	  /* Previously did not trim, but we could have added a splice to the end */
	} else if (low < distal5) { /* was path5->main_univdiagonal */
	  debug8(printf("Need to trim low part of 3' read, since %u < %u\n",low,distal5));
	  debug8(Path_print(path5));
	  debug8(Path_print(path3));
	  valid3p = Path_trim_qstart_trimdiag(path3,query3_compress_fwd,query3_compress_rev,
					      intlistpool,univcoordlistpool,
					      listpool,pathpool,transcriptpool,
					      /*trimdiag*/distal5);
	  debug8(printf("Result of trimming\n"));
	  debug8(Path_print(path3));
	}
	
	if (valid5p == false || valid3p == false) {
	  /* Skip: delete below */
#if 0
	} else if (Path_resolved_qend_p(path5) == true && Path_resolved_qstart_p(path3) == true) {
	  debug8(printf("Have a resolved inner: ")); debug8(Path_print(path5)); debug8(Path_print(path3));
#endif
	} else {
	  /* Need to call even if resolved, because of overreach issues */
	  debug8(printf("Calling Pathpair_resolve\n"));
	  if (Pathpair_resolve(&(*found_score_5),&(*found_score_3),
			       pathpair,pathpair->plusp,/*genestrand*/0,
			       query5_compress_fwd,query5_compress_fwd,query5_compress_rev,
			       query3_compress_fwd,query3_compress_fwd,query3_compress_rev,
			       /*queryseqL*/queryseq5,/*queryseqH*/queryseq3,queryuc_ptr_5,queryuc_ptr_3,
			       /*querylengthL*/querylength5,/*querylengthH*/querylength3,
			       novel_univdiags_alloc,novel_diagonals_alloc,localdb_alloc,
			       stage1_5,stage1_3,streamspace_max_alloc,streamspace_alloc,
			       knownsplicing,nmismatches_allowed_5,nmismatches_allowed_3,
			       intlistpool,univcoordlistpool,listpool,pathpool,
			       transcriptpool,univdiagpool,vectorpool,pass) == false) {
	    valid5p = valid3p = false; /* Delete below */
	  }
	  debug8(printf("Done with Pathpair_resolve\n"));
	}
	
      } else {
	/* minus */
	distal5 = Path_genomichigh(path5);
	distal3 = Path_genomiclow(path3);
	
	low = Univcoordlist_head(path5->univdiagonals);
	debug8(printf("low5 %u vs distal3 %u\n",low,distal3));

	if (0 && path3->transcriptome_method_p == true) {
	  /* Previously did not trim, but we could have added a splice to the end */
	} else if (low < distal3) { /* was path3->main_univdiagonal */
	  debug8(printf("Need to trim low part of 5' read, since %u < %u\n",low,distal3));
	  debug8(Path_print(path5));
	  debug8(Path_print(path3));
	  valid5p = Path_trim_qstart_trimdiag(path5,query5_compress_fwd,query5_compress_rev,
					      intlistpool,univcoordlistpool,
					      listpool,pathpool,transcriptpool,
					      /*trimdiag*/distal3);
	  debug8(printf("Result of trimming\n"));
	  debug8(Path_print(path5));
	}
	
	/* Need to shift 3' univdiagonals down by querylength3 */
	if (Univcoordlist_last_value(path3->univdiagonals) < (Univcoord_T) querylength3) {
	  high = 0;
	} else {
	  high = Univcoordlist_last_value(path3->univdiagonals) - querylength3;
	}
	debug8(printf("high3 %u vs distal5 %u\n",low,distal5));
	
	if (0 && path5->transcriptome_method_p == true) {
	  /* Previously did not trim, but we could have added a splice to the end */
	} else if (high > distal5) { /* was path5->main_univdiagonal */
	  debug8(printf("Need to trim high part of 3' read, since %u > %u\n",high,distal5));
	  debug8(Path_print(path5));
	  debug8(Path_print(path3));
	  valid3p = Path_trim_qend_trimdiag(path3,query3_compress_fwd,query3_compress_rev,
					    intlistpool,univcoordlistpool,
					    listpool,pathpool,transcriptpool,
					    querylength3,/*trimdiag*/distal5);
	  debug8(printf("Result of trimming\n"));
	  debug8(Path_print(path3));
	}
	
	if (valid5p == false || valid3p == false) {
	  /* Skip: delete below */
#if 0
	} else if (Path_resolved_qend_p(path3) == true && Path_resolved_qstart_p(path5) == true) {
	  debug8(printf("Have a resolved inner: ")); debug8(Path_print(path5)); debug8(Path_print(path3));
#endif
	} else {
	  /* Need to call even if resolved, because of overreach issues */
	  debug8(printf("Calling Pathpair_resolve\n"));
	  if (Pathpair_resolve(&(*found_score_5),&(*found_score_3),
			       pathpair,pathpair->plusp,/*genestrand*/0,
			       query3_compress_rev,query3_compress_fwd,query3_compress_rev,
			       query5_compress_rev,query5_compress_fwd,query5_compress_rev,
			       /*queryseqL*/queryseq3,/*queryseqH*/queryseq5,queryrc3,queryrc5,
			       /*querylengthL*/querylength3,/*querylengthH*/querylength5,
			       novel_univdiags_alloc,novel_diagonals_alloc,localdb_alloc,
			       stage1_3,stage1_5,streamspace_max_alloc,streamspace_alloc,
			       knownsplicing,nmismatches_allowed_3,nmismatches_allowed_5,
			       intlistpool,univcoordlistpool,listpool,pathpool,
			       transcriptpool,univdiagpool,vectorpool,pass) == false) {
	    valid5p = valid3p = false; /* Delete below */
	  }
	  debug8(printf("Done with Pathpair_resolve\n"));
	}
      }
      
      if (valid5p == false || valid3p == false) {
	Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
		      listpool,pathpool,transcriptpool,hitlistpool);
      } else {
	pathpairarray[k++] = pathpairarray[i];
      }
    }

    if ((npaths = k) == 0) {
      *npaths_primary = *npaths_altloc = 0;
      *first_absmq = 0;
      *second_absmq = 0;
      return (Pathpair_T *) NULL;
    }

    /* 2b.  Can get structurally identical results after Pathpair_resolve, so need to repeat step 1 */
    if (npaths > 1) {
      duplicatep = (bool *) CALLOC(npaths,sizeof(bool));
      /* qsort(pathpairarray,npaths,sizeof(Pathpair_T),Pathpair_interval_cmp); -- already sorted at step 1 */
      
      k = 0;
      i = 0;
      while (i < npaths) {
	j = i + 1;
	while (j < npaths && Pathpair_overlap_p(pathpairarray[j],pathpairarray[i]) == true) {
	  j++;
	}
      
	if (j - i == 1) {
	  pathpairarray[k++] = pathpairarray[i];
	  
	} else {
	  debug8(printf("Found an overlapping group of %d => eliminating duplicates\n",j - i));
	  for (l = i; l < j; l++) {
	    if (duplicatep[l] == true) {
	      /* Already found to be a duplicate */
	    } else {
	      for (ll = l + 1; ll < j; ll++) {
		if (Pathpair_structure_cmp(&(pathpairarray[l]),&(pathpairarray[ll])) == 0) {
		  debug8(printf("Path %d is a duplicate of %d\n",ll,l));
		  duplicatep[ll] = true;
		}
	      }
	    }
	  }
	  
	  for (l = i; l < j; l++) {
	    pathpair = pathpairarray[l];
	    if (duplicatep[l] == false) {
	      pathpairarray[k++] = pathpair;
	    } else {
	      Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			    listpool,pathpool,transcriptpool,hitlistpool);
	    }
	  }
	}
	
	i = j;
      }
      
      FREE(duplicatep);
      npaths = k;
    }

    debug8(printf("(2) After removing antisense and sense duplicates separately, have %d paths\n",npaths));
  }
  

  /* Note: Don't maximize on nmatches until we find fusions, because a fusion splice may need to truncate nmatches */

  /* 3.  Check all solutions for a possible outer fusion */
  if (splicingp == true) {
    /* 3a.  Look for possibilities */
    for (i = 0; i < npaths; i++) {
      pathpair = pathpairarray[i];
      if (pathpair->plusp == true) {
	if (Path_unextended_qstart_p(pathpair->path5,/*endtrim_allowed*/25,/*allow_ambig_p*/false) == true) {
	  possible_fusion5_p = true;
	}
	if (Path_unextended_qend_p(pathpair->path3,/*endtrim_allowed*/25,/*allow_ambig_p*/false) == true) {
	  possible_fusion3_p = true;
	}
      } else {
	if (Path_unextended_qend_p(pathpair->path5,/*endtrim_allowed*/25,/*allow_ambig_p*/false) == true) {
	  possible_fusion5_p = true;
	}
	if (Path_unextended_qstart_p(pathpair->path3,/*endtrim_allowed*/25,/*allow_ambig_p*/false) == true) {
	  possible_fusion3_p = true;
	}
      }
      i++;
    }
  }
  

  debug8(printf("possible_fusion5_p %d, possible_fusion3_p %d\n",
		possible_fusion5_p,possible_fusion3_p));
  
  if (possible_fusion5_p == true || possible_fusion3_p == true) {
    /* 3b.  Look for outer fusions */
    
    if (possible_fusion5_p == true) {
      debug8(Stage1_list_extension(stage1_5));
    }
    
    if (possible_fusion3_p == true) {
      debug8(Stage1_list_extension(stage1_3));
    }
    
    paths = (List_T) NULL;
    unextended_pathpairs = (List_T) NULL;

    for (i = 0; i < npaths; i++) {
      pathpair = pathpairarray[i];
      
      /* Push original pathpair onto paths */
      paths = Hitlist_push(paths,hitlistpool,(void *) pathpair
			   hitlistpool_trace(__FILE__,__LINE__));
      
      /* Look for fusion paths */
      if (pathpair->plusp == true) {
	/* plus */
	if (Path_unextended_qstart_p(pathpair->path5,/*endtrim_allowed*/INNER_NEEDED,/*allow_ambig_p*/false) == false) {
	  fusion_paths_5 = (List_T) NULL;
	} else if (pathpair->path5->sensedir == SENSE_ANTI) {
	  /* Skip: requires sense */
	  fusion_paths_5 = (List_T) NULL;
	} else {
	  fusion_paths_5 = Path_fusion_outer_querystart_plus(&(*found_score_5),/*main*/pathpair->path5,stage1_5,

							     query5_compress_fwd,query5_compress_rev,
							     queryseq5,querylength5,knownsplicing,
							     pathpair->path5->genestrand,nmismatches_allowed_5,
							     intlistpool,uintlistpool,univcoordlistpool,
							     listpool,univdiagpool,pathpool,vectorpool,
							     transcriptpool,hitlistpool);
	}

	if (Path_unextended_qend_p(pathpair->path3,/*endtrim_allowed*/INNER_NEEDED,/*allow_ambig_p*/false) == false) {
	  fusion_paths_3 = (List_T) NULL;
	} else if (pathpair->path3->sensedir == SENSE_ANTI) {
	  /* Skip: requires sense */
	  fusion_paths_3 = (List_T) NULL;
	} else {
	  fusion_paths_3 = Path_fusion_outer_queryend_plus(&(*found_score_3),/*main*/pathpair->path3,stage1_3,

							   query3_compress_fwd,query3_compress_rev,
							   queryseq3,querylength3,knownsplicing,
							   pathpair->path3->genestrand,nmismatches_allowed_3,
							   intlistpool,uintlistpool,univcoordlistpool,
							   listpool,univdiagpool,pathpool,vectorpool,
							   transcriptpool,hitlistpool);
	}

	if (fusion_paths_5 == NULL && fusion_paths_3 == NULL) {
	  /* Skip */

	} else if (fusion_paths_5 != NULL && fusion_paths_3 == NULL) {
	  for (p = fusion_paths_5; p != NULL; p = List_next(p)) {
	    if ((new_pathpair = Pathpair_new_concordant(&unextended_pathpairs,
						       /*pathL*/(Path_T) List_head(p),/*pathH*/pathpair->path3,
						       /*queryseqL*/queryseq5,/*queryseqH*/queryseq3,/*plusp*/true,
						       nmismatches_filter_5,nmismatches_filter_3,
						       mincoverage_filter_5,mincoverage_filter_3,
						       intlistpool,univcoordlistpool,listpool,pathpool,vectorpool,
						       transcriptpool,hitlistpool,/*check_inner_p*/false,
							/*copyLp (was false)*/true,/*copyHp*/true)) != NULL) {
	      paths = Hitlist_push(paths,hitlistpool,(void *) new_pathpair
				   hitlistpool_trace(__FILE__,__LINE__));
	    }
	  }
	  /* Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
	     listpool,pathpool,transcriptpool,hitlistpool); */
	  Path_gc(&fusion_paths_5,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);

	} else if (fusion_paths_5 == NULL && fusion_paths_3 != NULL) {
	  for (p = fusion_paths_3; p != NULL; p = List_next(p)) {
	    if ((new_pathpair = Pathpair_new_concordant(&unextended_pathpairs,
							/*pathL*/pathpair->path5,/*pathH*/(Path_T) List_head(p),
							/*queryseqL*/queryseq5,/*queryseqH*/queryseq3,/*plusp*/true,
							nmismatches_filter_5,nmismatches_filter_3,
							mincoverage_filter_5,mincoverage_filter_3,
							intlistpool,univcoordlistpool,listpool,pathpool,vectorpool,
							transcriptpool,hitlistpool,/*check_inner_p*/false,
							/*copyLp*/true,/*copyHp (was false)*/true)) != NULL) {
	      paths = Hitlist_push(paths,hitlistpool,(void *) new_pathpair
				   hitlistpool_trace(__FILE__,__LINE__));
	    }
	  }
	  /* Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
	     listpool,pathpool,transcriptpool,hitlistpool); */
	  Path_gc(&fusion_paths_3,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);

	} else {
	  for (p = fusion_paths_5; p != NULL; p = List_next(p)) {
	    for (q = fusion_paths_3; q != NULL; q = List_next(q)) {
	      if ((new_pathpair = Pathpair_new_concordant(&unextended_pathpairs,
							  /*pathL*/(Path_T) List_head(p),/*pathH*/(Path_T) List_head(q),
							  /*queryseqL*/queryseq5,/*queryseqH*/queryseq3,/*plusp*/true,
							  nmismatches_filter_5,nmismatches_filter_3,
							  mincoverage_filter_5,mincoverage_filter_3,
							  intlistpool,univcoordlistpool,listpool,pathpool,vectorpool,
							  transcriptpool,hitlistpool,/*check_inner_p*/false,
							  /*copyLp (was false)*/true,/*copyHp (was false)*/true)) != NULL) {
		paths = Hitlist_push(paths,hitlistpool,(void *) new_pathpair
				     hitlistpool_trace(__FILE__,__LINE__));
	      }
	    }
	  }
	  /* Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
	     listpool,pathpool,transcriptpool,hitlistpool); */
	  Path_gc(&fusion_paths_5,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);
	  Path_gc(&fusion_paths_3,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);
	}

      } else {
	/* minus */
	if (Path_unextended_qstart_p(pathpair->path3,/*endtrim_allowed*/INNER_NEEDED,/*allow_ambig_p*/false) == false) {
	  fusion_paths_3 = (List_T) NULL;
	} else if (pathpair->path3->sensedir == SENSE_FORWARD) {
	  /* Skip: requires antisense */
	  fusion_paths_3 = (List_T) NULL;
	} else {
	  fusion_paths_3 = Path_fusion_outer_querystart_minus(&(*found_score_3),/*main*/pathpair->path3,stage1_3,

							      query3_compress_fwd,query3_compress_rev,
							      queryseq3,querylength3,knownsplicing,
							      pathpair->path3->genestrand,nmismatches_allowed_3,
							      intlistpool,uintlistpool,univcoordlistpool,
							      listpool,univdiagpool,pathpool,vectorpool,
							      transcriptpool,hitlistpool);
	}

	if (Path_unextended_qend_p(pathpair->path5,/*endtrim_allowed*/INNER_NEEDED,/*allow_ambig_p*/false) == false) {
	  fusion_paths_5 = (List_T) NULL;
	} else if (pathpair->path5->sensedir == SENSE_FORWARD) {
	  /* Skip: requires antisense */
	  fusion_paths_5 = (List_T) NULL;
	} else {
	  fusion_paths_5 = Path_fusion_outer_queryend_minus(&(*found_score_5),/*main*/pathpair->path5,stage1_5,

							    query5_compress_fwd,query5_compress_rev,
							    queryseq5,querylength5,knownsplicing,
							    pathpair->path5->genestrand,nmismatches_allowed_5,
							    intlistpool,uintlistpool,univcoordlistpool,
							    listpool,univdiagpool,pathpool,vectorpool,
							    transcriptpool,hitlistpool);
	}

	if (fusion_paths_5 == NULL && fusion_paths_3 == NULL) {
	  /* Skip */

	} else if (fusion_paths_5 != NULL && fusion_paths_3 == NULL) {
	  for (p = fusion_paths_5; p != NULL; p = List_next(p)) {
	    if ((new_pathpair = Pathpair_new_concordant(&unextended_pathpairs,
							/*pathL*/pathpair->path3,/*pathH*/(Path_T) List_head(p),
							/*queryseqL*/queryseq3,/*queryseqH*/queryseq5,/*plusp*/false,
							nmismatches_filter_5,nmismatches_filter_3,
							mincoverage_filter_5,mincoverage_filter_3,
							intlistpool,univcoordlistpool,listpool,pathpool,vectorpool,
							transcriptpool,hitlistpool,/*check_inner_p*/false,
							/*copyLp*/true,/*copyHp (was false)*/true)) != NULL) {
	      paths = Hitlist_push(paths,hitlistpool,(void *) new_pathpair
				   hitlistpool_trace(__FILE__,__LINE__));
	    }
	  }
	  /* Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
	     listpool,pathpool,transcriptpool,hitlistpool); */
	  Path_gc(&fusion_paths_5,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);

	} else if (fusion_paths_5 == NULL && fusion_paths_3 != NULL) {
	  for (p = fusion_paths_3; p != NULL; p = List_next(p)) {
	    if ((new_pathpair = Pathpair_new_concordant(&unextended_pathpairs,
							/*pathL*/(Path_T) List_head(p),/*pathH*/pathpair->path5,
							/*queryseqL*/queryseq3,/*queryseqH*/queryseq5,/*plusp*/false,
							nmismatches_filter_5,nmismatches_filter_3,
							mincoverage_filter_5,mincoverage_filter_3,
							intlistpool,univcoordlistpool,listpool,pathpool,vectorpool,
							transcriptpool,hitlistpool,/*check_inner_p*/false,
							/*copyLp (was false)*/true,/*copyHp*/true)) != NULL) {
	      paths = Hitlist_push(paths,hitlistpool,(void *) new_pathpair
				   hitlistpool_trace(__FILE__,__LINE__));
	    }
	  }
	  /* Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
	     listpool,pathpool,transcriptpool,hitlistpool); */
	  Path_gc(&fusion_paths_3,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);

	} else {
	  for (p = fusion_paths_5; p != NULL; p = List_next(p)) {
	    for (q = fusion_paths_3; q != NULL; q = List_next(q)) {
	      if ((new_pathpair = Pathpair_new_concordant(&unextended_pathpairs,
							  /*pathL*/(Path_T) List_head(q),/*pathH*/(Path_T) List_head(p),
							  /*queryseqL*/queryseq3,/*queryseqH*/queryseq5,/*plusp*/false,
							  nmismatches_filter_5,nmismatches_filter_3,
							  mincoverage_filter_5,mincoverage_filter_3,
							  intlistpool,univcoordlistpool,listpool,pathpool,vectorpool,
							  transcriptpool,hitlistpool,/*check_inner_p*/false,
							  /*copyLp (was false)*/true,/*copyHp (was false)*/true)) != NULL) {
		paths = Hitlist_push(paths,hitlistpool,(void *) new_pathpair
				     hitlistpool_trace(__FILE__,__LINE__));
	      }
	    }
	  }
	  /* Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
	     listpool,pathpool,transcriptpool,hitlistpool); */
	  Path_gc(&fusion_paths_5,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);
	  Path_gc(&fusion_paths_3,intlistpool,univcoordlistpool,listpool,pathpool,transcriptpool,hitlistpool);
	}
      }
    }

    /* Use new pathpairarray that integrates original paths and fusions */
    Pathpair_gc(&unextended_pathpairs,intlistpool,univcoordlistpool,listpool,pathpool,
		transcriptpool,hitlistpool);

    FREE_OUT(pathpairarray);
    pathpairarray = (Pathpair_T *) List_to_array_out_n(&npaths,paths);
    Hitlistpool_free_list(&paths,hitlistpool
			  hitlistpool_trace(__FILE__,__LINE__));
    debug8(printf("HAVE %d PATHS\n",npaths));
  }


  /* 4.  Find best sensedir for paths in each local region */
  /* Do not reward unnecessary splices, so do not eliminate alignments with splice prob of 0.0 */
  if (npaths > 1) {
    qsort(pathpairarray,npaths,sizeof(Pathpair_T),Pathpair_interval_cmp);

    k = 0;
    i = 0;
    while (i < npaths) {
      j = i + 1;
      while (j < npaths && Pathpair_overlap_p(pathpairarray[j],pathpairarray[i]) == true) {
	j++;
      }
	
      debug8(printf("Found an overlapping group of %d => choosing sense vs antisense\n",j - i));

      best_junction_prob_sense = best_junction_prob_antisense = 0.0;
      best_end_prob_sense = best_end_prob_antisense = 0.0;
      for (l = i; l < j; l++) {
	pathpair = pathpairarray[l];

	junction_prob = Path_avg_junction_prob(&end_prob,pathpair->pathL);
	junction_prob += Path_avg_junction_prob(&prob,pathpair->pathH);
	end_prob += prob;
	
	if (Pathpair_sensedir(pathpair) == SENSE_FORWARD) {
	  if (junction_prob > best_junction_prob_sense) {
	    best_junction_prob_sense = junction_prob;
	  }
	  if (end_prob > best_end_prob_sense) {
	    best_end_prob_sense = end_prob;
	  }

	} else {
	  if (junction_prob > best_junction_prob_antisense) {
	    best_junction_prob_antisense = junction_prob;
	  }
	  if (end_prob > best_end_prob_antisense) {
	    best_end_prob_antisense = end_prob;
	  }
	}
      }
	
      debug8(printf("best junction prob sense %f, end prob sense %f\n",
		    best_junction_prob_sense,best_end_prob_sense));
      debug8(printf("best junction prob antisense %f, end prob antisense %f\n",
		    best_junction_prob_antisense,best_end_prob_antisense));

      if (best_junction_prob_sense > best_junction_prob_antisense ||
	  (best_junction_prob_sense == best_junction_prob_antisense && best_end_prob_sense > best_end_prob_antisense)) {
	/* Keep only sense */
	for (l = i; l < j; l++) {
	  pathpair = pathpairarray[l];

	  junction_prob = Path_avg_junction_prob(&end_prob,pathpair->pathL);
	  junction_prob += Path_avg_junction_prob(&prob,pathpair->pathH);
	  end_prob += prob;
	  
#if 0
	  if (junction_prob == 0.0 && end_prob == 0.0) {
	    debug8(printf("(4 sensedir) Keeping null ")); debug8(Pathpair_print(pathpair));
	    pathpairarray[k++] = pathpair;
	  }
#endif
	  if (Pathpair_sensedir(pathpair) == SENSE_FORWARD) {
	    debug8(printf("(4 sensedir) Keeping sense ")); debug8(Pathpair_print(pathpair));
	    pathpairarray[k++] = pathpair;
	  } else {
	    debug8(printf("(4 sensedir) Eliminating antisense ")); debug8(Pathpair_print(pathpair));
	    Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			  listpool,pathpool,transcriptpool,hitlistpool);
	  }
	}

      } else if (best_junction_prob_antisense > best_junction_prob_sense ||
		 (best_junction_prob_antisense == best_junction_prob_sense && best_end_prob_antisense > best_end_prob_sense)) {
	/* Keep only antisense */
	for (l = i; l < j; l++) {
	  pathpair = pathpairarray[l];

	  junction_prob = Path_avg_junction_prob(&end_prob,pathpair->pathL);
	  junction_prob += Path_avg_junction_prob(&prob,pathpair->pathH);
	  end_prob += prob;

#if 0
	  if (junction_prob == 0.0 && end_prob == 0.0) {
	    debug8(printf("(4 sensedir) Keeping null ")); debug8(Pathpair_print(pathpair));
	    pathpairarray[k++] = pathpair;
	  }
#endif

	  if (Pathpair_sensedir(pathpair) == SENSE_ANTI) {
	    debug8(printf("(4 sensedir) Keeping antisense ")); debug8(Pathpair_print(pathpair));
	    pathpairarray[k++] = pathpair;
	  } else {
	    debug8(printf("(4 sensedir) Eliminating sense ")); debug8(Pathpair_print(pathpair));
	    Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			  listpool,pathpool,transcriptpool,hitlistpool);
	  }
	}

      } else {
	/* Keep both */
	for (l = i; l < j; l++) {
	  pathpair = pathpairarray[l];
	  pathpairarray[k++] = pathpair;
	}
      }
	
      i = j;
    }
    npaths = k;
  }


  /* 5.  Find best paths in each local region */
  if (npaths > 1) {
    /* Array should already be sorted */
    k = 0;
    i = 0;
    while (i < npaths) {
      j = i + 1;
      while (j < npaths && Pathpair_overlap_p(pathpairarray[j],pathpairarray[i]) == true) {
	j++;
      }
	
      debug8(printf("Found an overlapping group of %d => re-sorting by Pathpair_local_sort\n",
		    j - i));

      /* Keep the best ones in the overlapping group */
      qsort(&(pathpairarray[i]),j - i,sizeof(Pathpair_T),Pathpair_local_sort);
      debug8(printf("(5 local best) Keeping by local_sort ")); debug8(Pathpair_print(pathpairarray[i]));
      pathpairarray[k++] = pathpairarray[i];
	
      for (l = i + 1; l < j; l++) {
	pathpair = pathpairarray[l];
	if (Pathpair_structure_ignore_sense_cmp(&pathpair,&(pathpairarray[i])) == 0) {
	  debug8(printf("(5 local identical) Eliminating by identical structure ")); debug8(Pathpair_print(pathpair));
	  Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			listpool,pathpool,transcriptpool,hitlistpool);
	  
	} else if (Pathpair_local_cmp(pathpair,pathpairarray[i],
				      query5_compress_fwd,query5_compress_rev,
				      query3_compress_fwd,query3_compress_rev) <= 0) {
	  debug8(printf("(5 local tie or better) Keeping by local_cmp ")); debug8(Pathpair_print(pathpair));
	  pathpairarray[k++] = pathpair;
	    
	} else {
	  debug8(printf("(5 local worse) Eliminating by local_cmp ")); debug8(Pathpair_print(pathpair));
	  Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
			listpool,pathpool,transcriptpool,hitlistpool);
	}
      }
	
      i = j;
    }
    npaths = k;
  }


  /* 6.  Filter the result */
  if (filterp == true) {
    debug8(printf("Filtering with filterp %d.  max_nmismatches %d and %d, min_coverage %d and %d\n",
		  filterp,nmismatches_filter_5,nmismatches_filter_3,
		  mincoverage_filter_5,mincoverage_filter_3));
    k = 0;
    for (i = 0; i < npaths; i++) {
      pathpair = pathpairarray[i];
      if (pathpair->path5->score_within_trims > nmismatches_filter_5 || pathpair->path3->score_within_trims > nmismatches_filter_3) {
	debug8(printf("(6 filter by nmismatches) Eliminating ")); debug8(Pathpair_print(pathpair));
	Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
		      listpool,pathpool,transcriptpool,hitlistpool);
      } else if (Path_coverage(pathpair->path5) < mincoverage_filter_5 ||
		 Path_coverage(pathpair->path3) < mincoverage_filter_3) {
	debug8(printf("(6 filter by coverage %d or %d) Eliminating ",mincoverage_filter_5,mincoverage_filter_3));
	debug8(Pathpair_print(pathpair));
	Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
		      listpool,pathpool,transcriptpool,hitlistpool);
      } else if (allow_soft_clips_p == false &&
		 (Intlist_head(pathpair->path5->endpoints) != 0 || Intlist_last_value(pathpair->path5->endpoints) != querylength5 ||
		  Intlist_head(pathpair->path3->endpoints) != 0 || Intlist_last_value(pathpair->path3->endpoints) != querylength3)) {
	debug8(printf("(6 filter by soft clips) Eliminating ")); debug8(Pathpair_print(pathpair));
	Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
		      listpool,pathpool,transcriptpool,hitlistpool);
      } else {
	pathpairarray[k++] = pathpair;
      }
    }

    npaths = k;
  }

  if (npaths == 0) {
    FREE_OUT(pathpairarray);
    *npaths_primary = *npaths_altloc = 0;
    return (Pathpair_T *) NULL;

  } else if (npaths == 1) {
    /* No need to sort */

  } else {
    qsort(pathpairarray,npaths,sizeof(T),Pathpair_global_sort);

    /* 7.  Keep all paths equivalent to the best path */
    debug8(printf("(7 global best) Keeping best by global_cmp\n")); debug8(Pathpair_print(pathpairarray[0]));
    i = 1;		/* Skip the best path */
      
    for (k = 1; k < npaths; k++) {
      pathpair = pathpairarray[k];
      if (Pathpair_global_cmp(pathpair,pathpairarray[0],
			      query5_compress_fwd,query5_compress_rev,
			      query3_compress_fwd,query3_compress_rev) == 0) {
	debug8(printf("(7 global tie) Keeping by global_cmp\n")); debug8(Pathpair_print(pathpair));

	pathpairarray[i++] = pathpair;
      } else {
	debug8(printf("(7 global worse) Eliminating by global_cmp\n")); debug8(Pathpair_print(pathpair));
	Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
		      listpool,pathpool,transcriptpool,hitlistpool);
      }
    }

    npaths = i;			
  }

  debug8(printf("Found the global best solution across %d paths with %d + %d nmatches\n",
		npaths,pathpairarray[0]->path5->nmatches,pathpairarray[0]->path3->nmatches));

  if (want_random_p && npaths > 1) {
    /* Randomize among best alignments */
    /* randomi = (int) ((double) i * rand()/((double) RAND_MAX + 1.0)); */
    randomi = (int) (rand() / (((double) RAND_MAX + 1.0) / (double) npaths));
    /* fprintf(stderr,"%d dups => random %d\n",i,randomi); */
    debug8(printf("Swapping random path (%d out of %d) %p with best path %p\n",
		  randomi,npaths,pathpairarray[randomi],pathpairarray[0]));
    temp = pathpairarray[0];
    pathpairarray[0] = pathpairarray[randomi];
    pathpairarray[randomi] = temp;
  }


  /* Trim alignments at chromosomal bounds */
  for (i = 0; i < npaths; i++) {
    pathpair = pathpairarray[i];
    if (circularp[pathpair->pathL->chrnum] == false) {
#if 0
      /* Now done at the start of the procedure */
      Path_trim_qstart_trimbounds(pathpair->pathL,intlistpool,univcoordlistpool,listpool,pathpool,
				  /*trimbounds*/pathpair->pathL->chroffset);
#endif
    } else if (output_type == STD_OUTPUT || output_type == M8_OUTPUT) {
      /* If output type is alignment or m8, then don't want to split up the parts */
    } else if (pathpair->pathL->plusp == true) {
      Path_trim_circular(pathpair->pathL,query5_compress_fwd,intlistpool,univcoordlistpool,listpool);
    } else {
      Path_trim_circular(pathpair->pathL,query3_compress_rev,intlistpool,univcoordlistpool,listpool);
    }

    if (circularp[pathpair->pathH->chrnum] == false) {
#if 0
      Path_trim_qend_trimbounds(pathpair->pathH,intlistpool,univcoordlistpool,listpool,pathpool,
				/*trimbounds*/pathpair->pathH->chrhigh);
#endif
    } else if (output_type == STD_OUTPUT || output_type == M8_OUTPUT) {
      /* If output type is alignment or m8, then don't want to split up the parts */
    } else if (pathpair->pathH->plusp == true) {
      Path_trim_circular(pathpair->pathH,query3_compress_fwd,intlistpool,univcoordlistpool,listpool);
    } else {
      Path_trim_circular(pathpair->pathH,query5_compress_rev,intlistpool,univcoordlistpool,listpool);
    }
  }


  /* Compute mapq_loglik */
  if (npaths == 1) {
    pathpair = pathpairarray[0];

    Path_mark_alignment(pathpair->path5,query5_compress_fwd,queryuc_ptr_5,query5_compress_rev,queryrc5,
			pathpool);
    Path_mark_alignment(pathpair->path3,query3_compress_fwd,queryuc_ptr_3,query3_compress_rev,queryrc3,
			pathpool);

    pathpair->mapq_loglik = MAPQ_MAXIMUM_SCORE;
    pathpair->mapq_score = MAPQ_max_quality_score(quality_string_5,querylength5);
    if ((mapq_score = MAPQ_max_quality_score(quality_string_3,querylength3)) > pathpairarray[0]->mapq_score) {
      pathpair->mapq_score = mapq_score;
    }
    pathpair->absmq_score = MAPQ_MAXIMUM_SCORE;

    *first_absmq = pathpair->absmq_score;
    *second_absmq = 0;

  } else {
    for (i = 0; i < npaths; i++) {
      pathpair = pathpairarray[i];
      path5 = pathpair->path5;
      path3 = pathpair->path3;
      Path_mark_alignment(path5,query5_compress_fwd,queryuc_ptr_5,query5_compress_rev,queryrc5,
			  pathpool);
      Path_mark_alignment(path3,query3_compress_fwd,queryuc_ptr_3,query3_compress_rev,queryrc3,
			  pathpool);
      pathpair->mapq_loglik =
	MAPQ_loglik_string(path5->genomic_diff,quality_string_5,querylength5,path5->plusp);
      pathpair->mapq_loglik +=
	MAPQ_loglik_string(path3->genomic_diff,quality_string_3,querylength3,path3->plusp);
    }


    /* Enforce monotonicity */
    for (i = npaths - 1; i > 0; i--) {
      if (pathpairarray[i-1]->mapq_loglik < pathpairarray[i]->mapq_loglik) {
	pathpairarray[i-1]->mapq_loglik = pathpairarray[i]->mapq_loglik;
      }
    }
    maxlik = pathpairarray[0]->mapq_loglik;

    /* Subtract maxlik to avoid underflow */
    for (i = 0; i < npaths; i++) {
      pathpairarray[i]->mapq_loglik -= maxlik;
    }

    /* Compute absolute mapq */
    for (i = 0; i < npaths; i++) {
      loglik = pathpairarray[i]->mapq_loglik + MAPQ_MAXIMUM_SCORE;
      if (loglik < 0.0) {
	loglik = 0.0;
      }
      pathpairarray[i]->absmq_score = rint(loglik);
    }
    *first_absmq = pathpairarray[0]->absmq_score;
    if (npaths == 1) {
      *second_absmq = 0;
    } else {
      *second_absmq = pathpairarray[1]->absmq_score;
    }

    /* Compute Bayesian mapq */
    total = 0.0;
    for (i = 0; i < npaths; i++) {
      total += (pathpairarray[i]->mapq_loglik = fasterexp(pathpairarray[i]->mapq_loglik));
    }

    /* Obtain posterior probabilities of being true */
    for (i = 0; i < npaths; i++) {
      pathpairarray[i]->mapq_loglik /= total;
    }

    /* Convert to Phred scores */
    for (i = 0; i < npaths; i++) {
      if ((qual = 1.0 - pathpairarray[i]->mapq_loglik) < 2.5e-10 /* 10^-9.6 */) {
	pathpairarray[i]->mapq_score = 40;
      } else {
	pathpairarray[i]->mapq_score = rint(-10.0 * log10(qual));
      }
    }
  }


  /* Compute transcripts */
  if (transcriptome != NULL) {
    for (i = 0; i < npaths; i++) {
      pathpair = pathpairarray[i];
      path5 = pathpair->path5;
      path3 = pathpair->path3;

      Transcript_velocity_paired(path5,path3);
      if ((tr_insertlength = Transcript_fragment_length(path5,path3,queryseq5,queryseq3)) > 0) {
	pathpair->insertlength = tr_insertlength;
      }
    }
  }


  /* Filter for chrsubset */
  /* Want to allow other alignments to be found before filtering */
  *npaths_primary = *npaths_altloc = 0;
  if (chrsubsetp == NULL) {
    k = 0;
    for (pathi = 0; pathi < npaths; pathi++) {
      pathpair = pathpairarray[pathi];
      if (altlocp[pathpair->path5->chrnum] == true ||
	  altlocp[pathpair->path3->chrnum] == true) {
	(*npaths_altloc) += 1;
      } else {
	(*npaths_primary) += 1;
      }
    }

    return pathpairarray;

  } else {
    k = 0;
    for (pathi = 0; pathi < npaths; pathi++) {
      pathpair = pathpairarray[pathi];
      if (chrsubsetp[pathpair->path5->chrnum] == false ||
	  chrsubsetp[pathpair->path3->chrnum] == false) {
	/* Do not save this pathpair */
	Pathpair_free(&pathpair,intlistpool,univcoordlistpool,
		      listpool,pathpool,transcriptpool,hitlistpool);
	
      } else {
	/* Save this pathpair.  Re-use existing array */
	if (altlocp[pathpair->path5->chrnum] == true ||
	    altlocp[pathpair->path3->chrnum] == true) {
	  (*npaths_altloc) += 1;
	} else {
	  (*npaths_primary) += 1;
	}
	pathpairarray[k++] = pathpair;
      }
    }

    if ((*npaths_primary) + (*npaths_altloc) == 0) {
      FREE_OUT(pathpairarray);
      return (Pathpair_T *) NULL;
    } else {
      return pathpairarray;
    }
  }
}


void
Pathpair_eval_setup (int max_insertlength_in, Transcriptome_T transcriptome_in,
		     bool *circularp_in, bool *chrsubsetp_in, bool *altlocp_in,
		     Outputtype_T output_type_in,
		     bool splicingp_in, bool resolve_inner_p_in, bool want_random_p_in,
		     bool allow_soft_clips_p_in) {

  max_insertlength = max_insertlength_in;
  transcriptome = transcriptome_in;

  circularp = circularp_in;
  chrsubsetp = chrsubsetp_in;
  altlocp = altlocp_in;

  output_type = output_type_in;
  splicingp = splicingp_in;
  resolve_inner_p = resolve_inner_p_in;
  want_random_p = want_random_p_in;
  allow_soft_clips_p = allow_soft_clips_p_in;

  return;
}


void
Pathpair_eval_pass2_setup (int max_insertlength_in) {

  max_insertlength = max_insertlength_in;

  return;
}


